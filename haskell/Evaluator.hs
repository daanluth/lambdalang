{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE LambdaCase #-}
module Evaluator (runFile, runLine)
where

import qualified Data.Map as Map
import Control.Exception (handle, throw)
import Parser (AST (..), parse, alphaConvert, ParserException (..))
import Extras (Definitions, getFile)
import qualified Extras

isReducible :: AST -> Bool
isReducible (Application (Lambda _ _) _) = True
isReducible (Application (Name _) _) = False
isReducible (Application a _) = isReducible a
isReducible _ = False

substitute :: String -> AST -> AST -> AST
substitute k v ast@(Name x)
  | x == k = v
  | otherwise = ast
substitute k v (Application a b)
  | isReducible ast' = reduce ast'
  | otherwise = ast'
  where ast' = Application (substitute k v a) (substitute k v b)
substitute k v ast@(Lambda param body)
  | param == k = ast
  -- name collisions are taken care of beforehand, so
  -- alpha-conversion is not nessecary here.
  | otherwise = Lambda param $ substitute k v body

reduce :: AST -> AST
reduce (Application (Lambda param body) arg) = substitute param arg body
reduce ast@(Application (Name _) _) = ast
reduce (Application a b) =
  case (Application (reduce a) b) of
    ast@(Application (Lambda _ _) _) -> reduce ast
    ast -> ast
reduce ast = ast

replaceDefs :: Definitions -> AST -> AST
replaceDefs defs ast | Map.null defs = ast
replaceDefs defs ast@(Name sym)
  | Map.member sym defs = replaceDefs defs $ defs Map.! sym
  | otherwise = ast
replaceDefs defs ast@(Lambda param body) =
  Lambda param $ replaceDefs (Map.delete param defs) body
replaceDefs defs (Application a b) =
  Application (replaceDefs defs a) (replaceDefs defs b)
  
assignNonces :: AST -> AST
assignNonces = snd . helper 0
  where
    helper :: Int -> AST -> (Int, AST)
    helper n (Lambda param body) =
      let param' = param ++ "\\" ++ show n
          (n', body') = helper (n+1) body
      in (n', Lambda param' $ alphaConvert param param' body')
    helper n (Application a b) =
      let (n', a') = helper n a
          (n'', b') = helper n' b
      in (n'', Application a' b')
    helper n ast = (n, ast)

evaluate :: Definitions -> AST -> AST
evaluate defs =
  reduce .
  assignNonces .
  replaceDefs defs

runFile :: FilePath -> IO ()
runFile fpath = do
  (defs', exprs) <- getFile fpath
  let defs = replaceDefs defs' <$> defs'
  mapM_ (handle (\case (ParserException e) -> putStrLn ("[Parser Exception: \""++ e ++"\"]")) .
         print . evaluate defs)
    exprs

runLine :: Definitions -> String -> IO Definitions
runLine defs st
  | Extras.isImport st =
      Map.union defs <$> Extras.importFile (Extras.parseImport st)
  | Extras.isDefinition st =
    let (sym, ast) = Extras.parseDefinition st
    in return (Map.insert sym ast defs)
  | otherwise = do
      print $ evaluate defs $ parse st
      return defs
