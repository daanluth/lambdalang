{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

import qualified Data.Map as Map
import System.Environment (getArgs, getProgName)
import Evaluator (runFile)
import Extras (importFile)
import qualified Repl
import Common

helpMenu :: IO ()
helpMenu = do
  pname <- getProgName
  putStrLns [
    pname ++" [-h] [-l <file>] <file>",
    "  -h|--help: displays this help menu",
    "  -l|--load: loads a file and starts the REPL",
    "  <file>: the file to loaded or run.",
    "For example: ",
    "\""++ pname ++" -l lib.lam\" loads lib.lam and starts the REPL",
    "\""++ pname ++" main.lam\"   runs main.lam"]

main :: IO ()
main = do
  args <- getArgs
  case () of
    _ | null args -> Repl.start Map.empty
      | elem "-h" args || elem "--help" args -> helpMenu
      | head args == "-l" || head args == "--load" -> do
          defs <- importFile $ tail args
          Repl.start defs
      | otherwise -> mapM_ runFile args

