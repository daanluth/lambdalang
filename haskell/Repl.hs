{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE LambdaCase #-}
module Repl (start)
where

import Data.Char (toLower)
import Control.Exception (handle, throw)
import System.IO (hFlush, stdout)
import Parser (ParserException (..))
import Extras (Definitions)
import Evaluator (runLine)
import Common

licensePostfix :: IO ()
licensePostfix = putStrLns [
  "",
  "You should have received a copy of the GNU General Public License",
  "along with this program.  If not, see <https://www.gnu.org/licenses/>."]

showWarranty :: IO ()
showWarranty = do
  putStrLns [
    "This program is distributed in the hope that it will be useful,",
    "but WITHOUT ANY WARRANTY; without even the implied warranty of",
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the",
    "GNU General Public License for more details."]
  licensePostfix

showConditions :: IO ()
showConditions = do
  putStrLns [
    "This program is free software: you can redistribute it and/or modify",
    "it under the terms of the GNU General Public License as published by",
    "the Free Software Foundation, either version 3 of the License, or",
    "(at your option) any later version."]
  licensePostfix

repl :: Definitions -> IO ()
repl defs = do
  putStr "λ> "
  hFlush stdout
  line <- getLine
  case (map toLower line) of
    "" -> repl defs
    "\\warranty" -> do
      showWarranty
      repl defs
    "\\conditions" -> do
      showConditions
      repl defs
    otherwise ->
      handle (\case (ParserException e) -> do
                      putStrLn ("Parser Exception: \""++ e)
                      repl defs) $
      do
        defs' <- runLine defs line
        repl defs'

start :: Definitions -> IO ()
start defs = do
  putStrLns [
    "Lambda Lang REPL",
    "This program comes with ABSOLUTELY NO WARRANTY; for details type `\\warranty'.",
    "This is free software, and you are welcome to redistribute it",
    "under certain conditions; type `\\conditions' for details."]
  repl defs
