{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE LambdaCase #-}
module Parser (AST (..), ParserException (..), parse, alphaConvert)
where

import qualified Data.Map as Map
import qualified Data.Set as Set
import Control.Exception (throw)
import Common
import Lexer

data AST =
  Name String |
  Lambda String AST |
  Application AST AST
  deriving Eq

showLambda :: Map.Map String Int -> AST -> String
showLambda seen (Lambda param body)
  | Map.member param' seen =
    let param'' = param' ++"("++ show (seen Map.! param') ++")"
    in "\\"++ param'' ++"."++ showLambda
       (Map.adjust (+1) param' seen)
       (alphaConvert param param'' body)
  | otherwise =
    "\\"++ param' ++"."++ showLambda (Map.insert param' 1 seen) body
  where param' = showName param
showLambda _ ast = show ast

showName :: String -> String
showName = takeWhile (/= '\\')

instance Show AST where
  show (Name st) = showName st
  show ast@(Lambda _ _) = "("++ showLambda Map.empty ast ++")"
  show (Application a b@(Application _ _)) = show a ++" ("++ show b ++")"
  show (Application a b) = show a ++" "++ show b


alphaConvert :: String -> String -> AST -> AST
alphaConvert k v ast@(Lambda param body)
  | k == param = ast
  | otherwise = Lambda param $ alphaConvert k v body
alphaConvert k v (Application a b) =
  Application (alphaConvert k v a) (alphaConvert k v b)
alphaConvert k v ast@(Name n)
  | n == k = Name v
  | otherwise = ast


spanBlock :: [Lexeme] -> ([Lexeme],[Lexeme])
spanBlock ((Separator n):xs)
  | n < 0 = throw $ ParserException "Unbalanced Parentheses."
  | otherwise =
      next <$>
      span (\case (Separator m) -> m /= n
                  _ -> True) xs

rollApplication :: AST -> [Lexeme] -> AST
rollApplication ast xs@(Separator _:_) =
  let (block, after) =  spanBlock xs
  in rollApplication (Application ast $ parseAST block) after
rollApplication ast (Symbol x:xs) =
  rollApplication (Application ast $ Name x) xs
rollApplication ast [] = ast
rollApplication _ (Parameter sym:_) =
  throw $ ParserException ("Parsing error near parameter '"++ sym ++"'.")

parseAST :: [Lexeme] -> AST
parseAST xs@(Separator _:_) =
  let (block, after) = spanBlock xs
  in rollApplication (parseAST block) after
parseAST (Parameter x:xs) =
  Lambda x $ parseAST xs
parseAST (Symbol x:xs) = rollApplication (Name x) xs
parseAST [] = throw $ ParserException
  "General parser exception, possibly unbalanced parentheses"

parse :: String -> AST
parse = parseAST . lexify
