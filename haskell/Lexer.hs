{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Lexer (Lexeme (..), lexify, isSymbolChar)
where

import Common

data Lexeme =
  Separator Int |
  Parameter String |
  Symbol String
  deriving (Show)

lexify :: String -> [Lexeme]
lexify = helper 0
  where
    helper _ [] = []
    helper n ('(':cs) = Separator n : helper (n+1) cs
    helper n (')':cs) = Separator n' : helper n' cs
      where n' = n-1
    helper n ('\\':cs) =
      let (param, rst) = span isSymbolChar cs
      in Parameter param : helper n rst
    helper n (c:cs)
      | isSymbolChar c =
        let (sym, rst) = span isSymbolChar cs
        in Symbol (c:sym) : helper n rst
      | otherwise = helper n cs
