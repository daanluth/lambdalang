{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Common
where

import Control.Exception (Exception)

next :: [a] -> [a]
next [] = []
next (x:xs) = xs


isWhiteSpace :: Char -> Bool
isWhiteSpace ' ' = True
isWhiteSpace '\t' = True
isWhiteSpace _ = False

stripLeft :: String -> String
stripLeft = dropWhile isWhiteSpace
stripRight :: String -> String
stripRight = reverse . stripLeft . reverse

isSymbolChar :: Char -> Bool
isSymbolChar c
  | isWhiteSpace c = False
  | c=='\\' || c=='.' || c=='(' || c==')' = False
  | otherwise = True


newtype ParserException = ParserException String
  deriving Show
instance Exception ParserException


putStrLns :: [String] -> IO ()
putStrLns = mapM_ putStrLn
