{-
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}

module Extras (Definitions, getFile,
               parseImport, isImport, importFile,
               parseDefinition, isDefinition)
where

import qualified Data.Map as Map
import qualified Data.Set as Set
import Control.Exception (throw)
import Data.List (isPrefixOf, nub)
import Parser (AST (..), parse)
import Common

type Definitions = Map.Map String AST

isComment :: String -> Bool
isComment = (== '#') . head

isImport :: String -> Bool
isImport = isPrefixOf "import "

isDefinition :: String -> Bool
isDefinition = elem '='

parseImport :: String -> [FilePath]
parseImport = map (++ ".lam") . words . stripLeft . drop 7

parseDefinition :: String -> (String, AST)
parseDefinition st
  | null name = throw $ ParserException
    ("Empty name while parsing definition: '"++ st ++"'")
  | null l = throw $ ParserException
    ("Empty value while parsing definition: '"++ st ++"'")
  | not $ all isSymbolChar name = throw $ ParserException
    ("Invalid name in definition: '"++ name ++"'")
  | otherwise = (name, parse l)
  where (f, l) = stripLeft . next <$> span (/= '=') st
        name = stripRight f

-- sorts lines into imports, definitions and expressions
sortLines :: [String] -> ([String],[String],[String])
sortLines = helper ([],[],[])
  where
    helper out@(i,d,e) (x:xs)
      | null x || isComment x = helper out xs
      | isImport x = helper (x:i,d,e) xs
      | isDefinition x = helper (i,x:d,e) xs
      -- the expressions should stay in order, so they can be
      -- evaluated in order.
      | otherwise = helper (i,d,e ++ [x]) xs
    helper x [] = x

parseLines :: [String] -> ([FilePath], Definitions, [AST])
parseLines xs =
  (concatMap parseImport imports,
   foldl (\r (k, v) -> Map.insert k v r) Map.empty $
    map parseDefinition defs,
   map parse exprs)
  where (imports, defs, exprs) = sortLines $ map stripLeft xs


-- File import handling
realImportPath :: FilePath -> FilePath -> FilePath
realImportPath base fpath
  | head fpath == '/' = fpath
  | otherwise = basedir ++ fpath
  where basedir = reverse $ dropWhile (/= '/') $ reverse base

importFile' :: Set.Set FilePath -> [FilePath] -> IO Definitions
importFile' seen files =
  foldl Map.union Map.empty <$>
  mapM (\fpath -> do
          (imports', defs, _) <- parseLines . lines <$> readFile fpath
          -- avoid dependency loops, only import things once
          let imports = filter (not . (`Set.member` seen)) $
                        nub $ map (realImportPath fpath) imports'
              seen' = foldl (flip Set.insert) seen imports
          -- recursively import all the new files that this one points to
          defs' <- importFile' seen' imports
          return (Map.union defs defs'))
  files

importFile :: [FilePath] -> IO Definitions
importFile files = importFile' (Set.fromList files) files

-- All of the above
getFile :: FilePath -> IO (Definitions, [AST])
getFile fpath = do
  (imports, defs, exprs) <- parseLines . lines <$> readFile fpath
  defs' <- importFile $ map (realImportPath fpath) imports
  return (Map.union defs defs', exprs)
