(ns lambdalang.core-test
  (:require [clojure.test :refer :all]
            [lambdalang.core :refer :all]))

(deftest evaluation
  (let [correct
        {"(\\x.x) a" "a"
         "(\\x.\\y.x y) (\\i.i) b" "b"
         "(\\p.\\q.p q p) (\\x.\\y.x) (\\x.\\y.x)" "(\\x.\\y.x)"
         "(\\p.\\a.\\b.p a b) (\\x.\\y.x) a b" "a"
         "(\\p.\\a.\\b.p a b) ((\\p.\\q.p q p) (\\x.\\y.x) (\\x.\\y.y)) a b" "b"
         "(\\m.\\n.\\f.\\x.m f (n f x)) (\\f.\\x.f (f x)) (\\f.\\x.f x)" "(\\f.\\x.f (f (f x)))"}]

    (doall
     (for [[k v] correct]
       (is (= v (->> k
                     lambdalang.parser/parse
                     (lambdalang.evaluator/evaluate {})
                     lambdalang.parser/pprint-ast)))))))
