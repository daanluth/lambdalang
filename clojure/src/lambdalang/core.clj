;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns lambdalang.core
  (:require [clojure.string :as string])
  (:use lambdalang.extras)
  (:gen-class))

(declare show-conditions)
(declare show-warranty)
(defn- repl
  "Runs the REPL"
  [defs]
  (print "λ> ")
  (flush)
  (let [line (read-line)]
    (cond
      (empty? line) (repl defs)
      (= (string/upper-case line) "\\CONDITIONS") (do (show-conditions) (repl defs))
      (= (string/upper-case line) "\\WARRANTY") (do (show-warranty) (repl defs))
      true
      (try
        (repl (run-line defs line))
        (catch java.lang.NullPointerException e nil)))))

(defn- license-postfix
  "Common end part of the conditions / warranty" []
  (println "")
  (println "You should have received a copy of the GNU General Public License"),
  (println "along with this program.  If not, see <https://www.gnu.org/licenses/>."))

(defn- show-warranty
  "Shows warranty information" []
  (println "This program is distributed in the hope that it will be useful,"),
  (println "but WITHOUT ANY WARRANTY; without even the implied warranty of"),
  (println "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"),
  (println "GNU General Public License for more details.")
  (license-postfix))

(defn- show-conditions
  "Shows software redistribution conditions" []
  (println "This program is free software: you can redistribute it and/or modify")
  (println "it under the terms of the GNU General Public License as published by")
  (println "the Free Software Foundation, either version 3 of the License, or")
  (println "(at your option) any later version.")
  (license-postfix))

(defn- start-repl
  "Prints the REPL preamble and starts the REPL" [defs]
  (println "Lambda Lang REPL"),
  (println "This program comes with ABSOLUTELY NO WARRANTY; for details type `\\warranty'.")
  (println "This is free software, and you are welcome to redistribute it")
  (println "under certain conditions; type `\\conditions' for details.")
  (repl defs))

(defn- help-menu
  "Displays the CLI help menu"
  []
  (let [pname (if-let [v (java.lang.System/getProperty "program.name")] v "<progname>")]
    (println pname "[-h] [-l <file>] <file>")
    (println "  -h|--help: displays this help menu")
    (println "  -l|--load: loads a file and starts the REPL")
    (println "  <file>: the file to loaded or run.")
    (println "For example: ")
    (println (str "\"" pname) "-l lib.lam\" loads lib.lam and starts the REPL")
    (println (str "\"" pname) "main.lam\"   runs main.lam")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (cond
    (empty? args) (start-repl {})
    (some #{"--help" "-h"} args) (help-menu)
    (#{"--load" "-l"} (first args)) (start-repl (import-files (rest args)))
    true (dorun (map run-file args))))
