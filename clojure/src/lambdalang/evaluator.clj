;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns lambdalang.evaluator
  (:require [lambdalang.parser :as parser])
  (:use lambdalang.utils))

(defn- substitute
  "Turns any {:name $k} into $v in the AST,
  if $k is not a parameter of a surrounding function."
  [k v ast]
  (if-let [n (:name ast)]
    (if (= k (:name ast))
      v ast)
    (if-let [param (:lambda ast)]
      ;; check if $k is a parameter of this (surrounding) function
      (if (not= k param)
        (update ast :body (partial substitute k v)) ast)
      ;; substitute in both sides of the application
      (-> ast
          (update :application (partial substitute k v))
          (update :arg (partial substitute k v))))))

(defn- reduce-ast-CBN
  "Does beta-reduction on the AST, in call by name order"
  ([ast] (reduce-ast-CBN ast []))
  ([ast path]
   (let [curr (get-in ast path)]
     (if-let [app (:application curr)]
       (if-let [param (:lambda app)]
         ;; substitute the arguments into the body of the abstraction
         (let [substituted (substitute param (:arg curr) (:body app))]
           (if (empty? path)
             (recur substituted path)
             (recur (assoc-in ast path substituted) (pop path))))
         ;; if it is a application, we should evaluate that first
         ;; (so the innermost, leftmost one is evaluated first)
         (if (:application app)
           (recur ast (conj path :application))
           ;; otherwise, backtrack or just return the result
           ;; if there are :application s above this one,
           ;; remove those too, they can't proceed because the bottom one
           ;; is applying a :name to something
           (let [backtracked
                 (if (= :application (peek path))
                   (pop-while (partial = :application) path)
                   (if-not (empty? path) (pop path) path))]
             (if (empty? backtracked) ast
                 (recur ast backtracked)))))
       (if (empty? path) ast
           (recur ast (pop path)))))))

(defn- reducible?
  "true if the AST is reducible, nil otherwise"
  [ast]
  (when-let [app (:application ast)]
    (if (:lambda app) true
        (reducible? app))))

(defn- find-reducible
  "Returns the path to the first found reducible part of the AST,
  also looks inside function bodies, see reducible?"
  ([ast] (find-reducible ast []))
  ([ast path]
   (when-not (:name ast)
     (if (reducible? ast) path
         (if-let [app (:application ast)]
           ;; it's a application, and the reducible bit could
           ;; be in the argument or the operand.
           (if-let [p (find-reducible (:arg ast) (conj path :arg))]
             p (recur app (conj path :application)))
           ;; it's a lambda term
           (recur (:body ast) (conj path :body)))))))

(defn- reduce-ast
  [ast]
  ;; first reduce with call by name,
  (loop [curr (reduce-ast-CBN ast)]
    ;; if we are at a fixed point, stop
    ;; (this helps against infinitely expanding recursive functions,
    ;;  but also unfortunately leaves any function that can not be
    ;;  reduced any more with call by name unfinished.)
    (if (= ast curr) curr
        ;; find pieces that can be reduced further,
        ;; (so the thing presented at the end looks nicer and more concise)
        (if-let [path (find-reducible curr)]
          (recur (reduce-ast-CBN curr path))
          curr))))

(defn- replace-defs
  "Replaces the names with their definitions"
  [defs ast]
  (if (empty? defs) ast
      (casef ast
             :name
             #(if-let [def (get defs %)]
                (replace-defs defs def)
                ast)
             :lambda
             #(update ast :body (partial replace-defs (dissoc defs %)))
             :application
             (const
              (-> ast
                  (update :application (partial replace-defs defs))
                  (update :arg (partial replace-defs defs))))
             ast)))

(defn- assign-nonces
  "Assigns nonces to each name, to avoid name collisions and
  to remove the need for alpha-conversion"
  ([ast] (second (assign-nonces 0 ast)))
  ([n ast]
   (casef ast
          :lambda
          (fn [param]
            (let [[m body] (assign-nonces (inc n) (:body ast))
                  nparam (str param "\\" n)]
              (->> body
                   (alpha-convert param nparam)
                   (assoc (assoc ast :lambda nparam) :body)
                   (vector m))))
          :application
          (fn [a]
            (let [[n2 a2] (assign-nonces n a)
                  [n3 b] (assign-nonces n2 (:arg ast))]
              [n3 (-> ast
                     (assoc :application a2)
                     (assoc :arg b))]))
          [n ast])))

(defn evaluate
  [defs expr]
  (->> expr
       (replace-defs defs)
       assign-nonces
       reduce-ast))
