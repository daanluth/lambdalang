;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns lambdalang.parser
  (:require [clojure.string :as string])
  (:use lambdalang.utils))

(def ^:private name-char?
  "Checks weather or not the given character is a valid name character."
  (comp not #{\( \) \\ \. \tab \space}))

(defn- lexemes-helper
  "Helper function for lexemes"
  [n st]
  (when-let [c (first st)]
    (case c
      ;; a parameter
      \\ (let [[sym rst] (split-with name-char? (rest st))]
           (cons {:parameter (string/join sym)} (lexemes-helper n rst)))
      ;; a block of parentheses
      \( (let [[block xs] (split-with #(not (= (:paren %) n))
                                (lexemes-helper n (rest st)))]
           (cons block (rest xs)))
      ;; the end of a block
      \) (cons {:paren n} (lexemes-helper n (rest st)))
      ;; could be a name, otherwise just skip it
      (if (name-char? c)
        ;; register it as a name
        (let [[sym rst] (split-with name-char? st)]
          (cons {:name (string/join sym)} (lexemes-helper n rst)))
        ;; just ignore the character
        (recur n (rest st))))))
(def ^:private lexemes
  "Takes a string and returns lexemes/lexeme blocks from it"
  (partial lexemes-helper 0))

(declare parse-ast)
(defn- roll-application
  [ast xs]
  (if-let [x (first xs)]
    (roll-application
     ;; applications are left-associative
     {:application ast :arg (if (map? x) x (parse-ast x))}
     (rest xs))
    ast))

(defn- parse-ast
  "Parses the lexemes and returns a AST"
  [xs]
  (when-let [lexeme (first xs)]
    (if-not (map? lexeme)
      ;; its a block, parse inside the block and roll it into a application
      (roll-application (parse-ast lexeme) (rest xs))
      (if-let [p (:parameter lexeme)]
        ;; its a lambda
        {:lambda p :body (parse-ast (rest xs))}
        ;; its something else.
        (roll-application lexeme (rest xs))))))

(def parse
  "Parses the string and returns a AST"
  (comp parse-ast lexemes))

(defn- pprint-name
  "Pretty-prints the name, discarding any invalid symbols"
  [st]
  (apply str (take-while (partial not= \\) st)))

(declare pprint-ast)
(defn- pprint-lambda
  "Pretty-prints the lambda AST"
  [seen ast]
  (casef ast
         :lambda
         #(let [pname (pprint-name %)
                rpname (if-let [n (get seen pname)]
                         (str pname "(" n ")") pname)]
            (str "\\" rpname
                 "." (pprint-lambda
                      (update seen pname (fn [v] (if v (inc v) 1)))
                      (alpha-convert % rpname (:body ast)))))
         (pprint-ast ast)))

(defn pprint-ast
  "Pretty-prints the AST"
  [ast]
  (casef ast
         ;; {:lambda "x" :body {:name "x"}}} -> "\x.x"
         ;; {:lambda "x" :body {:lambda "y" :body {:name "x"}}} -> "\x.\y.x"
         :lambda
         (const (str "(" (pprint-lambda {} ast) ")"))
         ;; {:application {:name "x"} :arg {:name "y"}} -> "x y"
         ;; {:application {:name "x"} :arg
         ;;  {:application {:name "y"} :arg {:name "z"}}} -> "x (y z)"
         :application
         (fn [a]
           (let [b (:arg ast)]
             (casef b
                    :application
                    (const (str (pprint-ast a) " (" (pprint-ast b) ")"))
                    (str (pprint-ast a) " " (pprint-ast b)))))
         ;; {:name "x\0"} -> "x"
         :name pprint-name
         ;; otherwise
         (if (fn? ast)
           "[IFn]"
           "[INVALID AST]")))
