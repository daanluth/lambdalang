;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns lambdalang.extras
  (:require [lambdalang.parser :as parser]
            [lambdalang.evaluator :as evaluator]
            [clojure.string :as string])
  (:use lambdalang.utils))

(def ^:private definition?
  "Returns true if the string is a definition"
  (partial some (partial = \=)))
(def ^:private import?
  "Returns true if the string is a import statement"
  (partial (flip string/starts-with?) "import "))
(def ^:private comment?
  "Returns true if the string is a comment"
  (comp (partial = \#) first))

(defn- parse-def
  "Parses a definition string"
  [st]
  (let [[sym ast] (map string/join (split-with (partial not= \=) st))]
    {(string/trimr sym) (parser/parse (string/triml (subs ast 1)))}))
(defn- parse-defs
  "Parses multiple lines with definitions and compiles them into a map"
  [lines]
  (reduce merge (map parse-def lines)))

(defn- file-basepath
  "Returns the base path of a file, e.g. '../foo/bar.txt' -> '../foo/'"
  [fname]
  (->> fname
       reverse
       (drop-while (partial not= \/))
       reverse
       (apply str)))
(defn- parse-import
  "Returns the file names from a import statement, found in a specific file"
  [fname st]
  (let [fbase (file-basepath fname)]
    (->> (subs st 7)
         string/triml
         words
         (map #(if (= \/ (first %))
                 % (str fbase %)))
         (map #(str % ".lam")))))

(defn- sort-lines
  "Sorts lines into :imports, :definitions and :expressions"
  [lines]
  (group-by
   #(cond
      (import? %) :imports
      (definition? %) :definitions
      true :expressions)
   lines))

(defn- preprocess-lines
  "Does some preprocessing on each line, removes comments and empty lines"
  [lines]
  (->> lines
       (map string/triml)
       (remove #(or (empty? %) (comment? %)))))

(defn- get-lines
  "Gets all lines from the file, and sorts them"
  [filename]
  (-> (slurp-lines filename)
      preprocess-lines
      sort-lines))

(defn- import-files-helper
  "Helper function for import-files"
  [seen filenames]
  (let [new-seen (reduce conj seen filenames)]
    (for [fname filenames
          :let [{import-lines :imports def-lines :definitions} (get-lines fname)
                imports (mapcat (partial parse-import fname) import-lines)]]
      (merge (parse-defs def-lines)
             (reduce merge (import-files-helper new-seen imports))))))

(defn import-files
  "Recursively imports all symbols from the given file names"
  [filenames]
  (reduce merge (import-files-helper #{} filenames)))

(defn run-file
  "Runs the given file and prints the result to *out*"
  [filename]
  (let [lines (get-lines filename)
        defs (->> (:imports lines)
                  (mapcat (partial parse-import filename))
                  import-files
                  (merge (parse-defs (:definitions lines))))]
    (doseq [expr (:expressions lines)]
      (->> expr
           parser/parse
           (evaluator/evaluate defs)
           parser/pprint-ast
           println))))

(defn run-line
  "Takes some definitions, and a string to parse/execute,
  runs the line and returns new/updated definitions."
  [defs st]
  (cond
    (import? st)
    (->> st
         (parse-import ".")
         import-files
         (merge defs))
    (definition? st)
    (->> st
         parse-def
         (merge defs))
    true
    (do (->> st
             parser/parse
             (evaluator/evaluate defs)
             parser/pprint-ast
             println)
        defs)))
