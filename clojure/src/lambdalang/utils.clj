;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns lambdalang.utils
  (:require [clojure.java.io :as io]))

(defmacro casef
  "Like case, but checks if the first function returns something truthy,
and returns the application of the second function to that in case it is."
  ([e] nil)
  ([e x] x)
  ([e check f & clauses]
   (let [sym (gensym)]
     ;; if the check succeeds, we use this function with the result
     `(if-let [~sym (~check ~e)]
        (~f ~sym)
        (casef ~e ~@clauses)))))

(defmacro const
  "Wraps the argument in a function that takes one arg and discards it."
  [x] `(fn [~(gensym)] ~x))

(defmacro flip
  "Flips the function with 2 arguments around, so the second one comes first and vice versa"
  [f] (let [a (gensym)
            b (gensym)]
        `(fn [~a ~b] (~f ~b ~a))))


(defn slurp-lines
  "Returns the lines of text from the given file"
  [filename]
  (-> filename
      io/file
      io/reader
      line-seq))

(defn pop-while
  "Like drop-while, but pops from the collection"
  [f coll]
  (if-let [x (peek coll)]
    (if (f x) (recur f (pop coll)) coll)
    coll))

(def whitespace? "Set of all whitespace characters"
  #{\tab \space})

(defn words
  "returns all words (whitespace-delimited pieces) of the string" 
  [st]
  (->> st
       (partition-by whitespace?)
       (remove (comp whitespace? first))
       (map (partial apply str))))

(defn alpha-convert
  "Does alpha-conversion from k to v on the given AST"
  [k v ast]
  (casef ast
         :name #(if (= % k) (assoc ast :name v) ast)
         ;; only convert inside abstractions if the
         ;; name is not locally bound
         :lambda #(if (= % k) ast
                      (update ast :body (partial alpha-convert k v)))
         (-> ast
             (update :application (partial alpha-convert k v))
             (update :arg (partial alpha-convert k v)))))
