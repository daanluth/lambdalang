(defproject lambdalang "0.1.0-SNAPSHOT"
  :description "Lambda Language interpreter / REPL"
  :url "http://example.com/notapplicable"
  :license {:name "GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.gnu.org/licenses/#GPL"}
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :main ^:skip-aot lambdalang.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
