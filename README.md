# Lambda Language

### What is this?
A lambda calculus interpreter, in two different languages.  
I made this to learn more about the lambda calculus,  
since it seemed (and, as i have discovered, is) quite cool.  
The syntax supports lambdas, applications and variable names  
and also definitions, file imports and comments.  

There are two versions, one in haskell and one in clojure.  
The one in clojure is a bit slower because i had to work around  
the lack of (automagically optimized) mutual recursion.  
There are probably ways to make the clojure version faster,  
but it works fine and i can't be bothered.  
(feel free to fork and improve though).  


### Usage:
`main a.lam`, evaluates `a.lam`  
`main a.lam b.lam` evaluates files `a.lam` and `b.lam` in order  
`main -l a.lam b.lam` loads `a.lam` and `b.lam` and starts the [REPL](#REPL)  
`main` just starts the [REPL](#REPL)  

### Syntax:
- Lambdas:  
  `(\x.x)` is the identity function  
  `(\x.\y.x)` takes two parameters, and chooses the first  

- Function application:  
  `(\x.x) a` applies `a` to `(\x.x)`, reducing to `a`.  
  likewise, `(\x.\y.y) a b` reduces to `b`.  
  Functions with multiple arguments with the same name will  
  be shown like `(\x.\x.\y.\x.x)` -> `(\x.\x(1).\y.\x(2).x(2))`.  

- Names:  
  `foo` is the name "foo".  
  **Note**: names may not contain whitespace, "(", ")", "\" or "."  

- File imports:  
  `import *` will recursively import  
  definitions from the specified files:  
  `import /opt/lib` imports from `/opt/lib.lam`  
  `import lib` imports from `./lib.lam`  

- Definitions:  
  `I = (\x.x)` defines `I` to be `(\x.x)`,  
  so a expression like `id a` is expanded to  
  `(\x.x) a`, which reduces to `a`.   
  No definitions are included by default.

- Comments:  
  `#` starts a comment, so `# this is a comment.`  
  Note: inline comments are not supported.  

#### In General:
If some way of writing things is unambigous, the syntax supports it.  
so ` a = \x.x` is valid, and so is `b  = (\x\y.(y x))`,  
but `\x.\y.x \z.z y` is not, and is ambiguous,  
(it could have meant either `\x.\y.x (\z.z y)` or `\x.\y.x (\z.z) y`)  

#### Additional Notes:
There is no safeguard against trying to  
show recursive / looping expressions (like `(\x.x x) (\x.x x)` or `Y f`),  
which will just continue being expanded indefinitely,  
until the stack space runs out. You can safely apply them though.

Dependency loops are silently ignored and worked around,  
and if two files contain a definitions with the same name,  
the last one imported is used.  

### REPL
The REPL is kind of bad, since there is no  
history functionality, and the arrow/special keys don't work properly.  
It is possible to make that work with haskell, but i felt like  
this would be a unnessecary and large diversion from the original goal,  
which was learning about the lambda calculus. So i didn't do it.  

If you are (as i was) annoyed by this, consider using [rlwrap](https://pkgs.org/search/?q=rlwrap).  

### Legal:
The programs are [licensed](LICENSE.md) under the [GNU GPL version 3](https://www.gnu.org/licenses/gpl-3.0.html).  
Basically, feel free to use, copy or redistribute any part of this,  
as long as you include the license and follow its terms.  
Also, don't use my name for anything commercial without asking me.  
