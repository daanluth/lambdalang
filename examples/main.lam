# file imports
import examplelib

# definitions
id = (\x.x)
# this will yield 'NAME'
id NAME
# imported definitions work too (I = (\x.x) in stdlib)
I NAME

# Application is bracketed to show the order
# f (a b) != (f a) b
# f a b   == (f a) b

# This will be interpreted as a variable name, and echoed back,
# which is nice because it can be used as a visual divider of the expressions
--------------[NUMBERS]----------------------------

# church numerals up to 10 are in the examplelib
1
2
3

----[these_expressions_will_all_evaluate_to_16]----

plus 10 6
mult 2 8
sub (mult 10 2) 4
# this one will look a bit weird, but is (alpha-)equivalent to 16,
# and shows that numbers are added to the  variable names to avoid ambiguity.
pow 4 2

--------------[BOOLEAN LOGIC]----------------------

true
false
----[logical_operators]----------------------------
and false true
and true true
----
or true false
or false false
----
xor true true
xor false false
xor true false
----
not true
not false
---[conditionals]----------------------------------
iff true a b
iff (and true false) a b

--------------[LOGIC_+_NUMBERS]--------------------
iszero 0
iszero 3
----
leq 3 5
leq 4 7
leq 8 3
----
max 3 2
max 5 9
--
min 3 2
min 5 9

--------------[MISC]-------------------------------

# standard combinators
I
K
S
B
C
W
U
Y

# using the Y combinator we can recursively define factorial
factorial = Y (\f.\n.iff (iszero n) 1 (mult n (f (pred n))))
# or the fibonacci numbers
fib = Y (\f.\n.iff (leq n 1) 1 (plus (f (pred n)) (f (pred (pred n)))))

# you can't actually show the function itself, since it will be expanded
# indefinitely (until the stack space on your machine runs out)
# factorial

# The same is true for statements like
# (\x.x x) (\x.x x)
# which will loop indefinitely, and so no result can be shown.

# but you can safely apply it.
---[applying_factorial]----------------------------
factorial 1
factorial 2
factorial 3
factorial 4

---[applying_fibonacci]----------------------------
fib 0
fib 1
fib 2
fib 3
fib 4
fib 5
fib 6
fib 7